﻿using System.Data.Entity;
using Demo.Core;

namespace Demo.CompositionRoot
{
    public class DatabaseConfig
    {
        public static void SetInitializer()
        {
            Database.SetInitializer<DiagramDBContext>(new DropCreateDatabaseIfModelChanges<DiagramDBContext>());
        }
    }
}
