﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;

namespace Demo.CompositionRoot
{
    public class NinjectControllerFactory: DefaultControllerFactory
    {
        private readonly IKernel _ninjectKernel;

        public NinjectControllerFactory()
        {
            _ninjectKernel = NinjectWebCommon.bootstrapper.Kernel;
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {

            return (controllerType == null) ? null : (IController)_ninjectKernel.Get(controllerType);
        }
    }
}
