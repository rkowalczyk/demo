﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Demo.Model;


namespace Demo.Core
{
    public class DiagramDBContext : DbContext
    {
        public DiagramDBContext()
            : base("DatabaseContext")
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public virtual DbSet<Diagram> Diagrams { get; set; }
        public virtual DbSet<Node> Nodes { get; set; }
        public virtual DbSet<Link> Links { get; set; }
    }
}
