﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.IO;
using System.Threading.Tasks;
using Demo.Model;
using System.Web;
using System.Xml.Serialization;

namespace Demo.Core.Repositories
{
    public class FileRepository : IRepository<Diagram>
    {
        private readonly string _fileName;
        private readonly string _rootDirectory;

        public FileRepository()
        {
            _fileName = "diagramDraft.xml";
            _rootDirectory = HttpContext.Current.Server.MapPath("~/UserFiles");
        }

        public IEnumerable<Diagram> Get()
        {
            throw new NotImplementedException();
        }

        public Task<List<Diagram>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public Diagram GetById(int id)
        {
            string targetDir = Path.Combine(_rootDirectory, id.ToString(), "Draft");
            string targetFile = Path.Combine(targetDir, _fileName);
            if (!Directory.Exists(targetDir))
            {
                Directory.CreateDirectory(targetDir);
            }
            Diagram diagramDraft = null;
            if (File.Exists(targetFile))
                using (FileStream fs = new FileStream(targetFile, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Diagram));
                    diagramDraft = (Diagram)serializer.Deserialize(fs);
                }
            return diagramDraft;

        }

        public Task<Diagram> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<Diagram> GetByIdWithIncludeAsync(int id, params string[] include)
        {
            throw new NotImplementedException();
        }

        public Diagram Insert(Diagram entity)
        {
            throw new NotImplementedException();
        }

        public Task<Diagram> InsertAsync(Diagram entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            string userPath = Path.Combine(_rootDirectory, id.ToString());
            string draftPath = Path.Combine(_rootDirectory, id.ToString(), "Draft");
            string imagesPath = Path.Combine(_rootDirectory, id.ToString(), "Images");

            DirectoryInfo imagesDir = new DirectoryInfo(imagesPath);
            DirectoryInfo draftDir = new DirectoryInfo(draftPath);
            DirectoryInfo userDir = new DirectoryInfo(userPath);


            if (Directory.Exists(imagesPath))
            {
                foreach (FileInfo fi in imagesDir.GetFiles())
                {
                    fi.Delete();
                }
            }

            if (Directory.Exists(draftPath))
            {
                foreach (FileInfo fi in draftDir.GetFiles())
                {
                    fi.Delete();
                }
            }
        }

        public Task<int> DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Diagram Update(Diagram entityToUpdate, int id)
        {
            string targetDir = Path.Combine(_rootDirectory, id.ToString(), "Draft");
            string targetFile = Path.Combine(targetDir, _fileName);
            if (!Directory.Exists(targetDir))
            {
                Directory.CreateDirectory(targetDir);
            }
            if (File.Exists(targetFile))
                File.Delete(targetFile);

            XmlSerializer serializer = new XmlSerializer(typeof(Diagram));
            using (TextWriter writer = new StreamWriter(targetFile))
            {
                serializer.Serialize(writer, entityToUpdate);
                writer.Close();
            }
            return entityToUpdate;
        }

        public Task<Diagram> UpdateAsync(Diagram entityToUpdate, int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Diagram> GetMany(Func<Diagram, bool> @where)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Diagram> GetManyQueryable(Func<Diagram, bool> @where)
        {
            throw new NotImplementedException();
        }

        public Task<List<Diagram>> GetWithIncludeNoTrackAsync(Func<Diagram, bool> predicate, params string[] include)
        {
            throw new NotImplementedException();
        }

        public Task<int> Delete(Func<Diagram, bool> @where)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Diagram> GetWithInclude(Func<Diagram, bool> predicate, params string[] include)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Diagram> GetWithInclude(Expression<Func<Diagram, bool>> predicate, params string[] include)
        {
            throw new NotImplementedException();
        }

        public bool Exists(int primaryKey)
        {
            throw new NotImplementedException();
        }

        public Diagram GetSingle(Func<Diagram, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public Diagram GetFirst(Func<Diagram, bool> predicate)
        {
            throw new NotImplementedException();
        }
    }
}
