﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Demo.Model;

namespace Demo.Core.Repositories
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity: ModelBase 
    {
        #region
        internal DiagramDBContext Context;
        internal DbSet<TEntity> DbSet;
        #endregion

        #region

        public GenericRepository(DiagramDBContext context):base()
        {
            this.Context = context;
            this.DbSet = context.Set<TEntity>();
        }
        #endregion
        public IEnumerable<TEntity> Get()
        {
            return DbSet.ToList();
        }

        public async Task<List<TEntity>> GetAllAsync()
        {
            return await DbSet.ToListAsync();
        }

        public TEntity GetById(int id)
        {
            return DbSet.Find(id);
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await DbSet.FindAsync(id);
        }

        public TEntity Insert(TEntity entity)
        {
            DbSet.Add(entity);
            Context.SaveChanges();
            return entity;
        }

        public async Task<TEntity> InsertAsync(TEntity entity)
        {
            DbSet.Add(entity);
            await Context.SaveChangesAsync();
            return entity;
        }

        public void Delete(int id)
        {
            var entityToDelete = DbSet.Find(id);
            Delete(entityToDelete);
            Context.SaveChanges();
        }

        public async Task<int> DeleteAsync(int id)
        {
            var entityToDelete = DbSet.Find(id);
            Delete(entityToDelete);
            return await Context.SaveChangesAsync();
        }

        private void Delete(TEntity entityToDelete)
        {
            if (Context.Entry(entityToDelete).State == EntityState.Detached)
            {
                DbSet.Attach(entityToDelete);
            }
            DbSet.Remove(entityToDelete);
        }

        public TEntity Update(TEntity entityToUpdate, int id)
        {
            if (entityToUpdate == null)
                return null;

            TEntity existing = DbSet.Find(id);
            if (existing != null)
            {
                Context.Entry(existing).CurrentValues.SetValues(entityToUpdate);
            }
            return existing;
        }

        public async Task<TEntity> UpdateAsync(TEntity entityToUpdate, int id)
        {
            if (entityToUpdate == null)
                return null;

            TEntity existing = await Context.Set<TEntity>().FindAsync(id);
            if (existing != null)
            {
                Context.Entry(existing).CurrentValues.SetValues(entityToUpdate);
                await Context.SaveChangesAsync();
            }
            return existing;
        }

        public IEnumerable<TEntity> GetMany(Func<TEntity, bool> @where)
        {
            return DbSet.Where(where).ToList();
        }

        public IQueryable<TEntity> GetManyQueryable(Func<TEntity, bool> @where)
        {
            return DbSet.Where(where).AsQueryable();
        }

        public async Task<int> Delete(Func<TEntity, bool> @where)
        {
            DbSet.RemoveRange(DbSet.Where(where));
            return await Context.SaveChangesAsync();
        }

        public Task<TEntity> GetByIdWithIncludeAsync(int id, params string[] include)
        {
            IQueryable<TEntity> query = DbSet;
            query = include.Aggregate(query, (current, inc) => current.Include(inc));
            return query.SingleOrDefaultAsync(e => e.Id == id);
        }

        public IQueryable<TEntity> GetWithInclude(Func<TEntity, bool> predicate, params string[] include)
        {
            IQueryable<TEntity> query = DbSet;
            query = include.Aggregate(query, (current, inc) => current.Include(inc));
            return query.Where(predicate).AsQueryable();
        }

        public Task<List<TEntity>> GetWithIncludeNoTrackAsync(Func<TEntity, bool> predicate, params string[] include)
        {
            IQueryable<TEntity> query = DbSet.AsNoTracking();
            query = include.Aggregate(query, (current, inc) => current.Include(inc));
            return query.Where(predicate).AsQueryable().ToListAsync();
        }

        public bool Exists(int primaryKey)
        {
            return DbSet.Find(primaryKey) != null;
        }

        public TEntity GetSingle(Func<TEntity, bool> predicate)
        {
            return DbSet.Single<TEntity>(predicate);
        }

        public TEntity GetFirst(Func<TEntity, bool> predicate)
        {
            return DbSet.First<TEntity>(predicate);
        }
    }
}
