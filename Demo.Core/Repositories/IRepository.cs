﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Demo.Model;

namespace Demo.Core.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>  
        /// generic Get method for Entities  
        /// </summary>  
        /// <returns></returns>  
        IEnumerable<TEntity> Get();

        Task<List<TEntity>> GetAllAsync();

        /// <summary>  
        /// Generic get method on the basis of id for Entities.  
        /// </summary>  
        /// <param name="id"></param>  
        /// <returns></returns>  
        TEntity GetById(int id);

        Task<TEntity> GetByIdAsync(int id);

        Task<TEntity> GetByIdWithIncludeAsync(int id, params string[] include);

        /// <summary>  
        /// generic Insert method for the entities  
        /// </summary>  
        /// <param name="entity"></param>
        /// <param name="id"></param>  
        TEntity Insert(TEntity entity);

        Task<TEntity> InsertAsync(TEntity entity);

        /// <summary>  
        /// Generic Delete method for the entities  
        /// </summary>  
        /// <param name="id"></param>  
        void Delete(int id);

        Task<int> DeleteAsync(int id);

        /// <summary>  
        /// Generic update method for the entities  
        /// </summary>  
        /// <param name="entityToUpdate"></param>  
        TEntity Update(TEntity entityToUpdate, int id);

        Task<TEntity> UpdateAsync(TEntity entityToUpdate, int id);

        /// <summary>  
        /// generic method to get many record on the basis of a condition.  
        /// </summary>  
        /// <param name="where"></param>  
        /// <returns></returns>  
        IEnumerable<TEntity> GetMany(Func<TEntity, bool> where);

        /// <summary>  
        /// generic method to get many record on the basis of a condition but query able.  
        /// </summary>  
        /// <param name="where"></param>  
        /// <returns></returns>  
        IQueryable<TEntity> GetManyQueryable(Func<TEntity, bool> where);

        Task<List<TEntity>> GetWithIncludeNoTrackAsync(Func<TEntity, bool> predicate, params string[] include);

        /// <summary>  
        /// generic delete method , deletes data for the entities on the basis of condition.  
        /// </summary>  
        /// <param name="where"></param>  
        /// <returns></returns>  
        Task<int> Delete(Func<TEntity, Boolean> where);

        /// <summary>  
        /// Inclue multiple  
        /// </summary>  
        /// <param name="predicate"></param>  
        /// <param name="include"></param>  
        /// <returns></returns>  
        IQueryable<TEntity> GetWithInclude(Func<TEntity, bool> predicate,
            params string[] include);

        /// <summary>  
        /// Generic method to check if entity exists  
        /// </summary>  
        /// <param name="primaryKey"></param>  
        /// <returns></returns>  
        bool Exists(int primaryKey);

        /// <summary>  
        /// Gets a single record by the specified criteria (usually the unique identifier)  
        /// </summary>  
        /// <param name="predicate">Criteria to match on</param>  
        /// <returns>A single record that matches the specified criteria</returns>  
        TEntity GetSingle(Func<TEntity, bool> predicate);

        /// <summary>  
        /// The first record matching the specified criteria  
        /// </summary>  
        /// <param name="predicate">Criteria to match on</param>  
        /// <returns>A single record containing the first record matching the specified criteria</returns>  
        TEntity GetFirst(Func<TEntity, bool> predicate);
    }
}
