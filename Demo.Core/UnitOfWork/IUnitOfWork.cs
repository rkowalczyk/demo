﻿using System.Threading.Tasks;
using Demo.Core.Repositories;
using Demo.Model;

namespace Demo.Core.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<Diagram> DiagramDbRepository { get; }
        IRepository<Node> NodeDbRepository { get; }
        IRepository<Link> LinkDbRepository { get; }

        IRepository<Diagram> DraftRepository { get; } 

        void Dispose();
        void Save();
        Task SaveAsync();
    }
}
