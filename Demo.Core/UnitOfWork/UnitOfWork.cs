﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Demo.Core.Repositories;
using Demo.Model;

namespace Demo.Core.UnitOfWork
{
    public class UnitOfWork:IUnitOfWork
    {
        private readonly DiagramDBContext _context = null;
        private IRepository<Diagram> _diagramDbRepository;
        private IRepository<Node> _nodeDbRepository;
        private IRepository<Link> _linkDbRepository;

        private IRepository<Diagram> _draftRepository;

        public UnitOfWork(DiagramDBContext context)
        {
            _context = context;
        }

        public IRepository<Diagram> DiagramDbRepository
        {
            get
            {
                if(this._diagramDbRepository == null)
                    this._diagramDbRepository = new GenericRepository<Diagram>(_context);
                return _diagramDbRepository;
            }
        }

        public IRepository<Node> NodeDbRepository
        {
            get
            {
                if (this._nodeDbRepository == null)
                    this._nodeDbRepository = new GenericRepository<Node>(_context);
                return _nodeDbRepository;
            }
        }

        public IRepository<Link> LinkDbRepository
        {
            get
            {
                if (this._linkDbRepository == null)
                    this._linkDbRepository = new GenericRepository<Link>(_context);
                return _linkDbRepository;
            }
        }

        public IRepository<Diagram> DraftRepository
        {
            get
            {
                if(this._draftRepository == null)
                    this._draftRepository = new FileRepository();
                return _draftRepository;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public Task SaveAsync()
        {
            return _context.SaveChangesAsync();
        }
    }
}
