﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.Model;
using Newtonsoft.Json.Linq;

namespace Demo.Interfaces.DatabaseService
{
    public interface IDatabaseService
    {
        Task<List<Diagram>> GetAllDiagramsInfo();
        Task<JObject> Save(JObject diagram, string name, string description, int diagramId);
        Task<JObject> Open(int diagramId);
        Task Delete(int id);
    }
}
