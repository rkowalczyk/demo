﻿using Demo.Model;
using Newtonsoft.Json.Linq;

namespace Demo.Interfaces.DraftService
{
    public interface IDraftService
    {
        void SaveDraft(JObject diagramModel, int diagramId);
        JObject LoadDraft(int diagramId);
        void ClearDraft(int diagramId);
    }
}
