﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;

namespace Demo.Model
{
    [XmlRoot("Diagram")]
    public class Diagram: ModelBase
    {
        #region ctrs
        public Diagram()
        {
            
        }

        public Diagram(JObject diagramJson, string name,  string description, int? diagramId)
        {
            Links = new List<Link>();
            Nodes = new List<Node>();
            if(diagramJson != null)
                ParseJson(diagramJson, name, description, diagramId);
        }
        #endregion

        [XmlAttribute]
        [StringLength(100)]
        public string Name { get; set; }
        [XmlAttribute]
        [StringLength(500)]
        public string Description { get; set; }
        [XmlAttribute]
        public string Class { get; set; }

        [XmlArray(IsNullable = true)]
        public virtual List<Link> Links { get; set; }
        [XmlArray(IsNullable = true)]
        public virtual List<Node> Nodes { get; set; }


        private void ParseJson(JObject jsonDiagramModel, string name, string description, int? diagramId)
        {
            this.Class = (string)jsonDiagramModel["class"];
            this.Name = name;
            this.Description = description;
            if (diagramId.HasValue)
            {
                this.Id = diagramId.Value;
            }

            var nodes = jsonDiagramModel["nodeDataArray"].ToArray();

            foreach (var node in nodes)
            {
                Node nodeModel = new Node
                {
                    GoJsKey = (int)node["key"],
                    Attributes =
                        string.Join(",", node.Where(e => ((JProperty)e).Name != "key").ToList()),
                };
                this.Nodes.Add(nodeModel);
            }

            var links = jsonDiagramModel["linkDataArray"].ToArray();

            foreach (var link in links)
            {
                Link linkModel = new Link();
                if (link["from"] != null)
                    linkModel.FromGoJsKey = (int)link["from"];
                if (link["to"] != null)
                    linkModel.ToGoJsKey = (int)link["to"];

                linkModel.Attributes = string.Join(",",
                    link.Where(e => ((JProperty)e).Name != "from" && ((JProperty)e).Name != "to").ToList());
                this.Links.Add(linkModel);
            }
        }

        public JObject JsonParse()
        {
            JObject model = new JObject
            {
                {"class", this.Class},
                {"Name", this.Name},
                {"Description", this.Description},
				{"Id", this.Id}
            };

            JArray nodes = new JArray();
            foreach (var node in this.Nodes)
            {
                JObject n = new JObject { { "key", node.GoJsKey } };
                JObject attributes = JObject.Parse("{" + node.Attributes + "}");
                foreach (var attr in attributes)
                {
                    n.Add(attr.Key, attr.Value);
                }
                nodes.Add(n);
            }
            model.Add("nodeDataArray", nodes);

            JArray links = new JArray();
            foreach (var link in this.Links)
            {
                JObject l = new JObject();
                if (link.FromGoJsKey.HasValue)
                    l.Add("from", link.FromGoJsKey);
                if (link.ToGoJsKey.HasValue)
                    l.Add("to", link.ToGoJsKey);


                JObject attributes = JObject.Parse("{" + link.Attributes + "}");
                foreach (var attr in attributes)
                {
                    l.Add(attr.Key, attr.Value);
                }

                links.Add(l);
            }
            model.Add("linkDataArray", links);

            return model;
        }
    }
}
