﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace Demo.Model
{
    public class Link: ModelBase
    {
        [XmlAttribute]
        public int DiagramId { get; set; }

        [XmlElement]
        public int? FromGoJsKey { get; set; }

        [XmlElement]
        public int? ToGoJsKey { get; set; }

        [XmlAttribute]
        public string Attributes { get; set; }

        [XmlIgnore]
        public virtual Diagram Diagram { get; set; }
    }
}
