﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Demo.Model
{
    public class Node: ModelBase
    {
        [XmlAttribute]
        public int DiagramId { get; set; }

        [XmlAttribute]
        public int GoJsKey { get; set; }

        [MaxLength]
        [XmlAttribute]
        public string Attributes { get; set; }

        [XmlIgnore]
        public virtual Diagram Diagram { get; set; }
    }
}
