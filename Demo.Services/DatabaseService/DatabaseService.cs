﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.Core.UnitOfWork;
using Demo.Interfaces.DatabaseService;
using Demo.Model;
using Newtonsoft.Json.Linq;

namespace Demo.Services.DatabaseService
{
    public class DatabaseService: ServiceBase, IDatabaseService
    {
        public DatabaseService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            
        }

        public Task<List<Diagram>> GetAllDiagramsInfo()
        {
            return UnitOfWork.DiagramDbRepository.GetAllAsync();
        }

        public async Task<JObject> Open(int diagramId)
        {
            Diagram model = await UnitOfWork.DiagramDbRepository.GetByIdWithIncludeAsync(diagramId, "Nodes", "Links");
            return model.JsonParse();
        }

        public Task Delete(int id)
        {
            return UnitOfWork.DiagramDbRepository.DeleteAsync(id);
        }

        public async Task<JObject> Save(JObject diagram, string name, string description, int diagramId)
        {
            Diagram model = new Diagram(diagram, name, description, diagramId);

            var diagramObj = UnitOfWork.DiagramDbRepository.GetById(diagramId);
            if (diagramObj != null && diagramObj.Name == name)
            {
                UnitOfWork.DiagramDbRepository.UpdateAsync(model, diagramId);
            }
            else
            {
                var savedEntity = await UnitOfWork.DiagramDbRepository.InsertAsync(model);
                model.Id = savedEntity.Id;
            }

            return model.JsonParse();
        }
    }
}
