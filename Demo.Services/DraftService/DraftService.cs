﻿using Demo.Core.UnitOfWork;
using Demo.Interfaces.DraftService;
using Demo.Model;
using Newtonsoft.Json.Linq;

namespace Demo.Services.DraftService
{
    public class DraftService: ServiceBase, IDraftService
    {
        public DraftService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            
        }
        public void SaveDraft(JObject diagramModel, int diagramId)
        {
            Diagram model = new Diagram(diagramModel, "tempName", "draftDiagram", diagramId);
            UnitOfWork.DraftRepository.Update(model, diagramId);   
        }

        public JObject LoadDraft(int diagramId)
        {
            var diagramDraft = UnitOfWork.DraftRepository.GetById(diagramId);
            return diagramDraft != null ? diagramDraft.JsonParse() : null;	        
        }

        public void ClearDraft(int diagramId)
        {
            UnitOfWork.DraftRepository.Delete(diagramId);
        }
    }
}
