﻿using Demo.Core.UnitOfWork;

namespace Demo.Services
{
    public abstract class ServiceBase
    {
        private readonly IUnitOfWork unitOfWork;

        protected ServiceBase(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        protected IUnitOfWork UnitOfWork
        {
            get { return this.unitOfWork; }
        }
    }
}
