﻿using System.Web.Optimization;

namespace Demo
{
    public class BundleConfig
    {

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Clear();
            bundles.ResetAll();

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/jquery-ui.min.css"));

            bundles.Add(new ScriptBundle("~/Scripts/gojs").Include(
                      "~/Scripts/GoJS/go.js"));

            bundles.Add(new ScriptBundle("~/Scripts/custom").Include(
                      "~/Scripts/Site/ApplicationEvent.js",
                      "~/Scripts/Site/ApplicationCore.js",
                      "~/Scripts/Site/Diagram.js"));

            bundles.Add(new ScriptBundle("~/Scripts/dataStructure").Include(
                      "~/Scripts/DataStructure/TabStructure.js"));

            //bundles.Add(new ScriptBundle("~/Scripts/dataStructure").Include(
            //          "~/Scripts/DataStructure/BetterTabStructure.js"));

            bundles.Add(new ScriptBundle("~/Scripts/tests").Include(
                      "~/Scripts/Tests/Tests.js"));


        }

    }
}