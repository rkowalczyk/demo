﻿using System.Collections.Generic;
using System.Linq;
using Demo.Interfaces.DraftService;
using System.Threading.Tasks;
using System.Web.Mvc;
using Demo.Interfaces.DatabaseService;
using System;
using Demo.Models;
using Newtonsoft.Json.Linq;

namespace Demo.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDraftService _draftService;
        private readonly IDatabaseService _databaseService;

        public HomeController(IDraftService draftService, IDatabaseService databaseService)
        {
            _draftService = draftService;
            _databaseService = databaseService;
        }

        public async Task<ActionResult> Index()
        {          
            return View();
        }

        public FileResult SaveImage(string Image)
        {
            var bytes = Convert.FromBase64String(Image);
            string fileName = "Diagram" + "_" + DateTime.Now.ToShortDateString() + ".jpg";

            return File(bytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public async Task<JsonResult> SaveDiagram(DiagramModel diagramModel)
        {
            JObject data = JObject.Parse(diagramModel.Data);
            diagramModel.Name = "NewDiagram" + DateTime.Now;
            int diagramId = diagramModel.Id.HasValue ? diagramModel.Id.Value : -1;
            await _databaseService.Save(data, diagramModel.Name, diagramModel.Description, diagramId);
            return Json(true);
        }

        public async Task<JsonResult> OpenDiagram(int Id)
        {
            JObject result = await _databaseService.Open(Id);
            return Json(result.ToString());
        }

        public async Task<JsonResult> GetDiagrams()
        {
            List<Demo.Model.Diagram> dbModel = await _databaseService.GetAllDiagramsInfo();
            var result = dbModel.Select(diagram => new DiagramModel
            {
                Name = diagram.Name,
                Description = diagram.Description,
                Id = diagram.Id
            }).ToList();
            
            return Json(result);
        }
    }
}