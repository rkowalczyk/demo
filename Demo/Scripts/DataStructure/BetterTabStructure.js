﻿TabStructure = function () {
    var pub = {};

    pub.currentTabName = "";
    pub.maxTabInd = 1;

    pub.models = {};

    pub.createTabs = function () {

        if (!jQuery.isEmptyObject(pub.models)) {

            $("#bottomTabs").empty();

            for (var i in pub.models) {
                var tab;
                if (i == pub.currentTabName) {
                    tab = "<div class='tab tabActive' onclick='TabStructure.tabChange(this);'>" + i + "</div>";
                }
                else {
                    tab = "<div class='tab' onclick='TabStructure.tabChange(this);'>" + i + "</div>";
                }

                $("#bottomTabs").append(tab);
            }
        }
    }

    pub.initTab = function () {

        pub.maxTabInd = 0;
        pub.currentTabName = "Model 1";

        pub.models["Model 1"] = {nodes: [], links: []};

        pub.createTabs();

    }

    pub.addTab = function () {
        pub.maxTabInd++;
        var newModelName = "Model " + (pub.maxTabInd + 1);
        var newModel = { nodes: [], links: [] };
        pub.models[newModelName] = newModel;

        pub.loadDiagramForCurrentTab(newModelName);
        pub.currentTabName = newModelName;

        pub.createTabs();
        Diagram.myDiagram.redraw();
    }

    pub.removeTab = function () {

        if (someModelsExist(pub.models)) {

            var result = {};
            var firstModelName = "";

            for (i in pub.models) {

                if (i != pub.currentTabName) {
                    result[i] = pub.models[i];

                    if (firstModelName == "") {
                        firstModelName = i;
                    }
                }
            }

            pub.models = result;
            pub.currentTabName = firstModelName;

            pub.createTabs();
        }
        else {
            alert("Tab could not be deleted");
        }

    }

    pub.tabChange = function (el) {
        var tabName = el.textContent;
        if (tabName != "" && tabName != pub.currentTabName) {

            $(".tab").removeClass("tabActive");
            $(el).addClass("tabActive");
            pub.loadDiagramForCurrentTab(tabName);
            pub.currentTabName = tabName;

        }
    }

    pub.loadDiagramForCurrentTab = function (newTabName) {

        if (pub.models[pub.currentTabName]) {

            pub.models[pub.currentTabName].nodes = Diagram.myDiagram.model.nodeDataArray;
            pub.models[pub.currentTabName].links = Diagram.myDiagram.model.linkDataArray;

        }
        
        Diagram.myDiagram.model.nodeDataArray = pub.models[newTabName] ? pub.models[newTabName].nodes : [];
        Diagram.myDiagram.model.linkDataArray = pub.models[newTabName] ? pub.models[newTabName].links : [];

    }

    //#region helpers

    function someModelsExist(object) {

        var number = 0;
        if (jQuery.isEmptyObject(object)) {
            return false;
        }
        else {
            for (var i in object) {

                number++;

                if (number > 1) {
                    return true;
                }

            }
        }

        return false;
    }

    //#endregion

    return pub;
}()
