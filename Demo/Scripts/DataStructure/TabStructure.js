﻿TabStructure = function () {
    var pub = {};
    
    pub.currentTabName = "";
    pub.maxTabInd = 1;

    pub.models = [];
    pub.nodes = [];
    pub.links = [];

    pub.createTabs = function(){
        
        if(pub.models && pub.models.length > 0){
            
            $( "#bottomTabs" ).empty();
            
            for(var i in pub.models){
                var tab;
                if(pub.models[i] == pub.currentTabName){
                    tab = "<div class='tab tabActive' onclick='TabStructure.tabChange(this);'>" + pub.models[i] + "</div>";
                }
                else{
                    tab = "<div class='tab' onclick='TabStructure.tabChange(this);'>" + pub.models[i] + "</div>";
                }
                
                $( "#bottomTabs" ).append(tab);
            }   
        }
    }

    pub.initTab = function(){
        
        pub.maxTabInd = 0;
        pub.currentTabName = "Model 1";

        pub.models = ["Model 1"];

        pub.createTabs();

    }

    pub.addTab = function(){
        pub.maxTabInd++;
        var newModelName =  "Model " + (pub.maxTabInd + 1);   

        pub.models.push(newModelName);

        pub.loadDiagramForCurrentTab(newModelName);
        pub.currentTabName = newModelName;
       
        pub.createTabs();

        Diagram.myDiagram.redraw();
    }

    pub.removeTab = function(){
        
        if(pub.models.length > 1){
            
            var result = [];
            
            for(i in pub.models){
                
                if(pub.models[i] != pub.currentTabName){
                    result.push(pub.models[i]);
                }                            
            }
            
            pub.models = result;
            pub.currentTabName = pub.models[0].tabName;          

            pub.createTabs();
        }
        else{
            alert("Tab could not be deleted");
        }
        
    }
    
    pub.tabChange = function(el){
        var tabName = el.textContent;
        if(tabName != "" && tabName != pub.currentTabName){
            
            $(".tab").removeClass("tabActive");
            $(el).addClass("tabActive");
            pub.loadDiagramForCurrentTab(tabName);
            pub.currentTabName = tabName;
               
        }
    }

    pub.loadDiagramForCurrentTab = function(newTabName){
        
        var tabNodes = [];
        var tabLinks = [];
        var currentNodes = [];
        var currentLinks = [];
        
        if(Diagram.myDiagram.model.nodeDataArray.length > 0){
            
            for(var i in Diagram.myDiagram.model.nodeDataArray){
                var node = Diagram.myDiagram.model.nodeDataArray[i];

                if(node["tabName"] === undefined){
                    node["tabName"] = pub.currentTabName;                   
                }                
                tabNodes.push(node);
            }          
        }

        if(Diagram.myDiagram.model.linkDataArray.length > 0){
            for(var i in Diagram.myDiagram.model.linkDataArray){
                var link = Diagram.myDiagram.model.linkDataArray[i];
                
                if(link["tabName"] === undefined){

                    link["tabName"] = pub.currentTabName;                    
                }                
                tabLinks.push(link);
            }
        }
        
        if(pub.nodes.length > 0 && newTabName != ""){
            for(var i in pub.nodes){
                if (pub.nodes[i]["tabName"] == newTabName) {
                    currentNodes.push(pub.nodes[i]);
                }
                else{
                    tabNodes.push(pub.nodes[i]);
                }
            }        
        }        
        
        if(pub.links.length > 0 && newTabName != ""){
            for(var i in pub.links){
                if (pub.links[i]["tabName"] == newTabName) {
                    currentLinks.push(pub.links[i]);
                }
                else{
                    tabLinks.push(pub.links[i]);
                }
            }        
        }

        pub.nodes = tabNodes;
        pub.links = tabLinks;

        Diagram.myDiagram.model.nodeDataArray = currentNodes;
        Diagram.myDiagram.model.linkDataArray = currentLinks;
        
    }

    return pub;
}()
