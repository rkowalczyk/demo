﻿ApplicationCore = function () {
    var pub = {};
    pub.projectId = -1;

    //#region Save Diagram
    pub.SaveDiagram = function () {
        var model = Diagram.myDiagram.model.toJSON();
        $.ajax({
            type: "POST",
            url: UrlAction("SaveDiagram", "Home"),
            data: { Name: "", Description: "", Id: pub.Id, Data: model },
            cache: false
        }).done(function (data) {
            alert("Diagram saved succesfully !");
        });
    }

    //#endregion

    //#region Open Diagram
    pub.OpenDiagram = function (id) {
        $.ajax({
            type: "POST",
            url: UrlAction("OpenDiagram", "Home"),
            data: { Id: id },
            cache: false
        }).done(function (data) {
            pub.Id = JSON.parse(data).Id;
            Diagram.myDiagram.model = go.Model.fromJSON(data);
            $("#OpenPopup").dialog("close");
        });
    }

    pub.showOpenPopup = function (data) {
        var dialogName = "OpenPopup";
        $("#" + dialogName).remove();
        var div = $("<div id='" + dialogName + "' title=''></div>");
        div.dialog({
            autoOpen: false,
            width: 300,
            height: 300
        });
        div.dialog("option", "position", { my: "center", at: "center", of: window });

        div.dialog({ close: function () { $(this).html(""); } });
        div.dialog('option', 'title', 'Open');
        div.dialog("open");
        var content = "<table class='openTable'><tr><th>Name</th><th>Action</th></tr>";
        data.forEach(function (el) {
            content += ("<tr><td>" + el.Name + "</td><td><a href='#' onclick='ApplicationCore.OpenDiagram(" + el.Id + ")'>Open</a></td></tr>");
        });
        content += "</table>";
        div.html(content);
    }

    pub.GetDiagrams = function () {
        $.ajax({
            type: "POST",
            url: UrlAction("GetDiagrams", "Home"),
            cache: false,
            contentType: "application/json; charset=utf-8"
        }).done(function (data) {
            pub.showOpenPopup(data);
        });
    }
    //#endregion

    //#region New Diagram
    pub.CreateNewDiagram = function () {

        Diagram.myDiagram.model.nodeDataArray = [];
        Diagram.myDiagram.model.linkDataArray = [];
        pub.projectId = -1;

    }
    //#endregion

    //#region Save As Image
    pub.SaveAsImage = function () {

        if (typeof Diagram.myDiagram != 'undefined') {
            var image = Diagram.myDiagram.makeImageData({ type: "image/png", background: "white", scale: 1, maxSize: new go.Size(Infinity, Infinity) });

            var temp = image.split(',');
            if (temp[1] == "") {
                image = Diagram.myDiagram.makeImageData({ type: "image/png", background: "white", scale: 0.6, maxSize: new go.Size(Infinity, Infinity) });
            }

            temp = image.split(',');
            if (temp[1] != "") {
                
                var formString = '<form action="' + "/Home/SaveImage" + '" method="post">';

                formString += '<input type="text" name="Image' + '" value="' + temp[1] + '" />';

                formString += '</form>';
                var form = $(formString);
                $('body').append(form);
                closeBrowserDialogFlag = 0;
                $(form).submit();
                window.setTimeout(function () { closeBrowserDialogFlag = 1; }, 1000);

            }
            else {
                alert("Diagram is too large to save it as an Image. Diagram's data will be saved without an image.");
            }
        }

    }
    //#endregion

    return pub;
}()