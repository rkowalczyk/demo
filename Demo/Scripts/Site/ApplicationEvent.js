﻿ApplicationEvent = function () {
    var pub = {};

    pub.setApplicationEvents = function () {

        $('#objectName').on("change", function () { Diagram.changeNodeText(jQuery(this).val()); });
        $('#objectType').on("change", function () { Diagram.changeNodeType(jQuery(this).val()); });
        $('#objectFontFamily').on("change", function () { Diagram.changeNodeFontFamily(jQuery(this).val()); });

    }

    pub.onNewDiagramClick = function () {
        ApplicationCore.CreateNewDiagram();
    }

    pub.onOpenDiagramClick = function () {
        ApplicationCore.GetDiagrams();
    }

    pub.onSaveDiagramClick = function () {
        ApplicationCore.SaveDiagram();
    }

    pub.onSaveDiagramAsImageClick = function () {
        ApplicationCore.SaveAsImage();
    }

    pub.onUndoClick = function () {

        Diagram.myDiagram.commandHandler.undo();

    }

    pub.onRedoClick = function () {

        Diagram.myDiagram.commandHandler.redo();

    }

    pub.onZoomInClick = function () {
        
        Diagram.setZoom(0.1);
    }

    pub.onZoomOutClick = function () {
        
        Diagram.setZoom(-0.1);
    }

    pub.onNewModelClick = function () {
        TabStructure.addTab();
    }

    pub.onRemoveModelClick = function () {
        TabStructure.removeTab();
    }

    return pub;
}()
