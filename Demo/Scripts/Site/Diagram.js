﻿Diagram = function () {
    var pub = {};
    
    //#region Diagram variables

    pub.myDiagram = {};
    pub.myPalette = {};
    pub.myOverview = {};

    // #endregion

    //#region diagram definition

    pub.initDiagram = function () {

        var $ = go.GraphObject.make;

        Diagram.myDiagram = $(go.Diagram, "myDiagram",
        {
            scrollMode: go.Diagram.InfiniteScroll,
            "undoManager.isEnabled": true,
            allowDrop: true,
            mouseDrop: function (e) {
                var diagramSelection = e.diagram.selection;
                var ok = Diagram.myDiagram.commandHandler.addTopLevelParts(diagramSelection, true);
                if (!ok) Diagram.myDiagram.currentTool.doCancel();
            }

        });

        Diagram.createNodeTemplates();

        Diagram.createLinkTemplate();
        Diagram.addDiagramEvents();
        Diagram.addGroupTemplates();
        Diagram.initDiagramOverview();

    }

    pub.initDataInDiagram = function () {

        Diagram.myDiagram.model = new go.GraphLinksModel(
            [
             { key: 1, category: "SimpleNode" }
            ],
            []
        );

    }
    

    pub.initDiagramOverview = function(){
        
        var $ = go.GraphObject.make;

        Diagram.myOverview =
        $(go.Overview, "myOverview",
            {
                observed: Diagram.myDiagram,
                contentAlignment: go.Spot.Center
        });
    }
    //#endregion

    //#region Diagram events

    pub.addDiagramEvents = function () {

        var $ = go.GraphObject.make;

        Diagram.myDiagram.addDiagramListener("ChangedSelection", onSelectionChanged);
        Diagram.myDiagram.addDiagramListener("LinkDrawn", onLinkDrawn);
    }

    function onSelectionChanged(e){
        var object = e.diagram.selection.first();
        
        if (object) {

            var name = object.data.headerText;
            var type = object.data.type;
            var font = object.data.font;
            var category = object.data.category;
            
            setPropertiesInput(true, name, type, font, category);
        }
        else{
            setPropertiesInput(false);
        }

    }

    function setPropertiesInput(isSelected, name, type, font, category) {

        if (isSelected) {
            jQuery("#objectName").val(name);
            jQuery("#objectType").val(type);
            jQuery("#objectFontFamily").val(getFontFamilyFromFontSettings(font));
            jQuery("#objectCategory").val(category);
        }
        else {
            jQuery("#objectName").val("");
            jQuery("#objectType").val("");
            jQuery("#objectFontFamily").val("");
            jQuery("#objectCategory").val("");
        }
    }


    function getFontFamilyFromFontSettings(fontSettings) {

        if (fontSettings && fontSettings != "") {

            var myRegex = /(bold |italic |normal )?(normal |initial |inherit |small-caps )?(normal |bold |bolder |lighter |initial |inherit |\d+ )*(\d+\w+)*[ ]*([\w+\d*\W*]+)*/i;

            var result = myRegex.exec(fontSettings);

            if (result && result[5]) {

                return result[5];

            }
        }
        
        return "sans-serif";
    }

    function onLinkDrawn(e) {

        var link = e.diagram.selection.first();

        if (link) {
            
            var fromNode = link.fromNode;
            var toNode = link.toNode;

            if (fromNode && toNode && fromNode.data.category === toNode.data.category) {
                Diagram.myDiagram.rollbackTransaction();
            }
        }
    }

    //#endregion

    //#region palette templates

    pub.initPalette = function () {

        var $ = go.GraphObject.make;

        Diagram.myPalette =
         $(go.Palette, "myPalette",
           {
               scale: 0.7,
               layout: $(go.GridLayout, { wrappingColumn: 1, spacing: new go.Size(30, 30) })
           });

        Diagram.createPaletteTemplates();
        Diagram.initDataInPalette();
        
    }

    pub.initDataInPalette = function () {

        Diagram.myPalette.model = new go.GraphLinksModel(
            [
             { key: 1, category: "SimpleNode", color: "green", headerText: "Green", borderColor: "black", font: "bold 14px sans-serif", type: "rectangle" },
             { key: 2, category: "SquareNode", color: "blue", headerText: "Blue", borderColor: "black", font: "bold 14px sans-serif", type: "square" },
             { key: 3, category: "TriangleNode", color: "#008080", headerText: "Teal", borderColor: "black", font: "bold 14px sans-serif", type: "triangle" },
             { key: 4, category: "GroupNode", isGroup: true }
            ],
            []
        );

    }

    pub.createPaletteTemplates = function(){
    
        var $ = go.GraphObject.make;

        Diagram.myPalette.nodeTemplateMap.add("SimpleNode",
            $(go.Node, "Spot",
                {
                    minSize: new go.Size(30, 30),
                    locationSpot: go.Spot.TopLeft,
                    resizable: true,
                    resizeObjectName: "Shape",
                    selectionObjectName: "Shape"
                },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, "RoundedRectangle",
                    {
                        name: "Shape",
                        fill: "white",
                        stroke: "black",
                        desiredSize: new go.Size(140, 70),
                        height: 70,
                        width: 140,
                        minSize: new go.Size(120, 57)
                    },
                    new go.Binding("fill", "color").makeTwoWay(),
                    new go.Binding("stroke", "borderColor").makeTwoWay(),
                    new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify)
                ),
                $(go.TextBlock,
                {
                    margin: 15,
                    editable: true,
                    width: 90,
                    isMultiline: true,
                    textAlign: "center",
                    font: 'bold 14px sans-serif',
                    text: 'header',
                    stroke: "black"
                },
                new go.Binding("text", "headerText").makeTwoWay(),
                new go.Binding("stroke", "fontColor").makeTwoWay(),
                new go.Binding("font", "font").makeTwoWay()
                )
            )
        );

        Diagram.myPalette.nodeTemplateMap.add("TriangleNode",
            $(go.Node, "Spot",
                {
                    minSize: new go.Size(30, 30),
                    locationSpot: go.Spot.TopLeft,
                    resizable: true,
                    resizeObjectName: "Shape",
                    selectionObjectName: "Shape"
                },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, "Triangle",
                    {
                        name: "Shape",
                        fill: "white",
                        stroke: "black",
                        desiredSize: new go.Size(140, 70),
                        height: 70,
                        width: 140,
                        minSize: new go.Size(120, 57)
                    },
                    new go.Binding("fill", "color").makeTwoWay(),
                    new go.Binding("stroke", "borderColor").makeTwoWay(),
                    new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify)
                ),
                $(go.TextBlock,
                {
                    margin: 15,
                    editable: true,
                    width: 90,
                    isMultiline: true,
                    textAlign: "center",
                    font: 'bold 14px sans-serif',
                    text: 'header',
                    stroke: "black"
                },
                new go.Binding("text", "headerText").makeTwoWay(),
                new go.Binding("stroke", "fontColor").makeTwoWay(),
                new go.Binding("font", "font").makeTwoWay()
                )
            )
        );

        Diagram.myPalette.nodeTemplateMap.add("SquareNode",
            $(go.Node, "Spot",
                {
                    minSize: new go.Size(30, 30),
                    locationSpot: go.Spot.TopLeft,
                    resizable: true,
                    resizeObjectName: "Shape",
                    selectionObjectName: "Shape"
                },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, "Square",
                    {
                        name: "Shape",
                        fill: "white",
                        stroke: "black",
                        desiredSize: new go.Size(70, 70),
                        height: 70,
                        width: 70,
                        minSize: new go.Size(60, 60)
                    },
                    new go.Binding("fill", "color").makeTwoWay(),
                    new go.Binding("stroke", "borderColor").makeTwoWay(),
                    new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify)
                ),
                $(go.TextBlock,
                {
                    margin: 15,
                    editable: true,
                    width: 90,
                    isMultiline: true,
                    textAlign: "center",
                    font: 'bold 14px sans-serif',
                    text: 'header',
                    stroke: "black"
                },
                new go.Binding("text", "headerText").makeTwoWay(),
                new go.Binding("stroke", "fontColor").makeTwoWay(),
                new go.Binding("font", "font").makeTwoWay()
                )
            )
        );
    
        Diagram.myPalette.groupTemplateMap.add("GroupNode",
                $(go.Node, go.Panel.Spot,
                {
                    locationSpot: go.Spot.Left,
                    padding: new go.Margin(0, 0, 10, 0)
                },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Panel, go.Panel.Vertical,
                    $(go.Panel, go.Panel.Vertical,
                      $(go.Shape, "Rectangle",
                        { fill: "#eee", stroke: "black", width: 35, height: 18, alignment: go.Spot.TopLeft, margin: new go.Margin(0, 0, -1, 0) }),
                      $(go.Shape, "Rectangle",
                        { fill: "#eee", stroke: "black", width: 70, height: 70 })
                    ),
                    $(go.TextBlock, "GroupNodeText",
                      {
                          name: "textBlock",
                          font: "normal 10px arial",
                          margin: 2,
                          stroke: "#000000",
                          text: "Group",
                          height: 30,
                          margin: 5,
                          width: 104,
                          wrap: go.TextBlock.WrapDesiredSize,
                          textAlign: 'center'
                      })
                   )
         ));
    }

    pub.createPaletteTemplatesWithoutBindings = function () {

        var $ = go.GraphObject.make;

        Diagram.myPalette.nodeTemplateMap.add("SimpleNode",
            $(go.Node, "Spot",
                {
                    minSize: new go.Size(30, 30),
                    locationSpot: go.Spot.TopLeft,
                    resizable: true,
                    resizeObjectName: "Shape",
                    selectionObjectName: "Shape"
                },
                $(go.Shape, "RoundedRectangle",
                    {
                        name: "Shape",
                        fill: "white",
                        stroke: "black",
                        desiredSize: new go.Size(140, 70),
                        height: 70,
                        width: 140,
                        portId: "",
                        minSize: new go.Size(120, 57)
                    }
                ),
                $(go.TextBlock,
                {
                    margin: 15,
                    editable: true,
                    width: 90,
                    isMultiline: true,
                    textAlign: "center",
                    font: 'bold 14px sans-serif',
                    text: 'header',
                    stroke: "black"
                }
                )
            )
        );
    }


    //#endregion

    //#region node templates

    pub.createNodeTemplates = function () {

        var $ = go.GraphObject.make;

        Diagram.myDiagram.nodeTemplateMap.add("SimpleNode",
            $(go.Node, "Spot",
                {
                    minSize: new go.Size(30, 30),
                    locationSpot: go.Spot.TopLeft,
                    resizable: true,
                    resizeObjectName: "Shape",
                    selectionObjectName: "Shape"
                },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, "RoundedRectangle",
                    {
                        name: "Shape",
                        fill: "white",
                        stroke: "black",
                        desiredSize: new go.Size(140, 70),
                        height: 70,
                        width: 140,
                        portId: "",
                        fromLinkable: true,
                        toLinkable: true,
                        minSize: new go.Size(120, 57)
                    },
                    new go.Binding("fill", "color").makeTwoWay(),
                    new go.Binding("stroke", "borderColor").makeTwoWay(),
                    new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify)
                ),
                $(go.TextBlock,
                {
                    margin: 15,
                    editable: true,
                    width: 90,
                    isMultiline: true,
                    textAlign: "center",
                    font: 'bold 14px sans-serif',
                    text: 'header',
                    stroke: "black"
                },
                new go.Binding("text", "headerText").makeTwoWay(),
                new go.Binding("stroke", "fontColor").makeTwoWay(),
                new go.Binding("font", "font").makeTwoWay()
                )
            )
        );

        Diagram.myDiagram.nodeTemplateMap.add("TriangleNode",
            $(go.Node, "Spot",
                {
                    minSize: new go.Size(30, 30),
                    locationSpot: go.Spot.TopLeft,
                    resizable: true,
                    resizeObjectName: "Shape",
                    selectionObjectName: "Shape"
                },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, "Triangle",
                    {
                        name: "Shape",
                        fill: "white",
                        stroke: "black",
                        desiredSize: new go.Size(140, 70),
                        height: 70,
                        width: 140,
                        portId: "",
                        fromLinkable: true,
                        toLinkable: true,
                        minSize: new go.Size(120, 57)
                    },
                    new go.Binding("fill", "color").makeTwoWay(),
                    new go.Binding("stroke", "borderColor").makeTwoWay(),
                    new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify)
                ),
                $(go.TextBlock,
                {
                    margin: 15,
                    editable: true,
                    width: 90,
                    isMultiline: true,
                    textAlign: "center",
                    font: 'bold 14px sans-serif',
                    text: 'header',
                    stroke: "black"
                },
                new go.Binding("text", "headerText").makeTwoWay(),
                new go.Binding("stroke", "fontColor").makeTwoWay(),
                new go.Binding("font", "font", function (el) {
                    return "bold 14px " + el;
                })
                )
            )
        );

        Diagram.myDiagram.nodeTemplateMap.add("SquareNode",
            $(go.Node, "Spot",
                {
                    minSize: new go.Size(30, 30),
                    locationSpot: go.Spot.TopLeft,
                    resizable: true,
                    resizeObjectName: "Shape",
                    selectionObjectName: "Shape"
                },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, "Square",
                    {
                        name: "Shape",
                        fill: "white",
                        stroke: "black",
                        desiredSize: new go.Size(70, 70),
                        height: 70,
                        width: 70,
                        portId: "",
                        fromLinkable: true,
                        toLinkable: true,
                        minSize: new go.Size(60, 60)
                    },
                    new go.Binding("fill", "color").makeTwoWay(),
                    new go.Binding("stroke", "borderColor").makeTwoWay(),
                    new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify)
                ),
                $(go.TextBlock,
                {
                    margin: 15,
                    editable: true,
                    width: 90,
                    isMultiline: true,
                    textAlign: "center",
                    font: 'bold 14px sans-serif',
                    text: 'header',
                    stroke: "black"
                },
                new go.Binding("text", "headerText").makeTwoWay(),
                new go.Binding("stroke", "fontColor").makeTwoWay(),
                new go.Binding("font", "font").makeTwoWay()
                )
            )
        );
    }

    pub.createNodeTemplatesWithoutBindings = function () {

        var $ = go.GraphObject.make;

        Diagram.myDiagram.nodeTemplateMap.add("SimpleNode",
            $(go.Node, "Spot",
                {
                    minSize: new go.Size(30, 30),
                    locationSpot: go.Spot.TopLeft,
                    resizable: true,
                    resizeObjectName: "Shape",
                    selectionObjectName: "Shape"
                },
                $(go.Shape, "RoundedRectangle",
                    {
                        name: "Shape",
                        fill: "white",
                        stroke: "black",
                        desiredSize: new go.Size(140, 70),
                        height: 70,
                        width: 140,
                        minSize: new go.Size(120, 57)
                    }
                ),
                $(go.TextBlock,
                {
                    margin: 15,
                    editable: true,
                    width: 90,
                    isMultiline: true,
                    textAlign: "center",
                    font: 'bold 14px sans-serif',
                    text: 'header',
                    stroke: "black"
                }
                )
            )
        );
    }

    //#endregion

    //#region group templates

    pub.addGroupTemplates = function () {

        var $ = go.GraphObject.make;
        
        Diagram.myDiagram.groupTemplateMap.add("GroupNode",
          $(go.Group, "Vertical",
            {
                locationSpot: go.Spot.Center,
                movable: true,
                copyable: true,
                deletable: true,
                selectionObjectName: "SHAPE",
                resizable: true,
                resizeObjectName: "SHAPE",
                mouseDrop: function (e, grp) {
                    var ok = grp.addMembers(grp.diagram.selection, true);
                    if (!ok) grp.diagram.currentTool.doCancel();
                }
            },
             new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),

            $(go.Panel, "Horizontal",
              {
                  alignment: go.Spot.TopLeft
              },
              $(go.Panel, go.Panel.Horizontal,
                {
                    background: "#ccc",
                    name: "Header"
                },
                $(go.TextBlock,
                    {
                        name: "textBlock",
                        font: "bold 12px arial",
                        margin: new go.Margin(3, 20, 3, 3),
                        desiredSize: new go.Size(370, 27),
                        editable: true
                    },
                    new go.Binding("text", "headerText").makeTwoWay(),
                    new go.Binding("font", "font").makeTwoWay())
              )
            ),
            $(go.Panel, go.Panel.Auto,
              $(go.Shape, "Rectangle",
                {
                    name: "SHAPE",
                    fill: "#fff",
                    desiredSize: new go.Size(400, 100)
                }
                )
            ) 
          ));
    }

    //#endregion 

    //#region link template

    pub.createLinkTemplate = function () {

        var $ = go.GraphObject.make;

        Diagram.myDiagram.linkTemplate =
        $(go.Link,
          {
              routing: go.Link.AvoidsNodes,
              curve: go.Link.JumpGap,
              toolTip:
                    $(go.Adornment, go.Panel.Auto,
                        $(go.Shape, { fill: "#FFFFCC" }),
                        $(go.TextBlock, { name: "ToolTipText", margin: 4, text: "Green relation" }, new go.Binding("text", "i2ToolTipText")))
          },
          $(go.Shape,
            {
                stroke: "green"
            },
            new go.Binding("stroke", "color").makeTwoWay()
          ),
          $(go.Shape,
            {
                toArrow: "Standard",
                stroke: "green"
            },
            new go.Binding("stroke", "color").makeTwoWay()
          ),
          $(go.Panel, "Auto",
            $(go.Shape, "RoundedRectangle",
            {
                figure: "RoundedRectangle",
                fill: "white",
                stroke: "green",
                minSize: new go.Size(10, 10)
            }),
            $(go.TextBlock,
            {
                textAlign: "center",
                editable: true,
                font: "bold 12px Roboto",
                stroke: "black",
                margin: 4,
                cursor: "pointer",
                text: "line"
            },
            new go.Binding("text", "headerText").makeTwoWay(),
            new go.Binding("font", "font").makeTwoWay())
            )
        );
    }

    //#endregion

    //#region changing object properties in right menu
    pub.changeNodeText = function(text){

        var object = Diagram.myDiagram.selection.first();

        if (object) {

            if (object.data.headerText != text) {

                Diagram.myDiagram.startTransaction("objectTextChanging");

                Diagram.myDiagram.model.setDataProperty(object.data, "headerText", text);

                Diagram.myDiagram.commitTransaction("objectTextChanging");

            }
        }
    }

    pub.changeNodeType = function (type) {

        var object = Diagram.myDiagram.selection.first();

        if (object) {

            if (object.data.type != type) {

                Diagram.myDiagram.startTransaction("objectTypeChanging");

                Diagram.myDiagram.model.setDataProperty(object.data, "type", type);

                Diagram.myDiagram.commitTransaction("objectTypeChanging");

            }
        }

    }

    pub.changeNodeFontFamily = function (fontFamily) {

        var object = Diagram.myDiagram.selection.first();

        if (object && fontFamily && fontFamily != "") {

            Diagram.myDiagram.startTransaction("objectFontChanging");

            if (object.data.category != "TraingleNode") {

                Diagram.myDiagram.model.setDataProperty(object.data, "font", "bold 14px " + fontFamily);             

            }
            else {

                Diagram.myDiagram.model.setDataProperty(object.data, "font", fontFamily);

            }

            Diagram.myDiagram.commitTransaction("objectFontChanging");
        }
    }
    //#endregion
    
    //#region diagram zoom 
    
    pub.setZoom = function(number){
        
        var newScale = Diagram.myDiagram.scale + number;
        
        if(newScale > 2){
            
            Diagram.myDiagram.scale = 2;  
        }
        else if( newScale < 0.1){
            Diagram.myDiagram.scale = 0.1;
        }
        else{
            Diagram.myDiagram.scale = newScale;
        }    
    
    }
    
    //#endregion
    
    return pub;
}()
