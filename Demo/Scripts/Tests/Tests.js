﻿DiagramTests = function () {
    var pub = {};

    //#region test structure
    pub.testTabStructure = function (category, color) {

        var nodes = [];
        var links = [];
        for (var i = 1; i < 900; i++) {

            var object1 = {
                borderColor: "black",
                category: category,
                color: color,
                font: "bold 14px sans-serif",
                headerText: "test1",
                key: i,
                loc: i * 2 + " " + 100,
                tabName: TabStructure.currentTabName,
                type: "rectangle",
            }

            var object2 = {
                borderColor: "black",
                category: category,
                color: color,
                font: "bold 14px sans-serif",
                headerText: "test2",
                key: i + 900,
                loc: i * 2 + " " + 400,
                tabName: TabStructure.currentTabName,
                type: "rectangle",
            }

            nodes.push(object1);
            nodes.push(object2);

            var link = { from: object1.key, to: object2.key };

            links.push(link);
        }

        Diagram.myDiagram.model.nodeDataArray = nodes;
        Diagram.myDiagram.model.linkDataArray = links;
    }
    //#endregion

    return pub;
}()
